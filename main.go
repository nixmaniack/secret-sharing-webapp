package main

import (
	"log"
	"net/http"

	"gitlab.com/nixmaniack/secret-sharing-webapp/handlers"
)

func main() {
	mux := http.NewServeMux()

	handlers.RegisterHandlers(mux)

	log.Println("Listening...")
	http.ListenAndServe(":8080", mux)
}
