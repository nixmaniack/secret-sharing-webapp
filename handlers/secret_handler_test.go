package handlers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestSecretHandlerNotAllowedMethod(t *testing.T) {
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("PUT", "/", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusMethodNotAllowed {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusMethodNotAllowed, writer.Code)
	}
}

func TestSecretHandlerPOSTInvalidJSON(t *testing.T) {
	writer := httptest.NewRecorder()
	payload := `invalid json}`
	request := httptest.NewRequest("POST", "/", strings.NewReader(payload))
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusBadRequest {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusBadRequest, writer.Code)
	}
}

func TestSecretHandlerPOSTJSONKeyMissing(t *testing.T) {
	writer := httptest.NewRecorder()
	payload := `{"foo": "secret"}`
	request := httptest.NewRequest("POST", "/", strings.NewReader(payload))
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusBadRequest {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusBadRequest, writer.Code)
	}

	expected := "Invalid request\n"
	got := writer.Body.String()
	if expected != got {
		t.Errorf("Expected Response Body to be: %T, Got: %T", expected, got)
	}
}

func TestSecretHandlerPOSTValid(t *testing.T) {
	// Create secret
	writer := httptest.NewRecorder()
	payload := `{"plain_text": "secret"}`
	request := httptest.NewRequest("POST", "/", strings.NewReader(payload))
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusOK {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusOK, writer.Code)
	}

	expected := `{"id":"5ebe2294ecd0e0f08eab7690d2a6ee69"}`
	got := writer.Body.String()
	if expected != got {
		t.Errorf("Expected Response Body to be: %s, Got: %s", expected, got)
	}

	// Retrieve secret first time
	writer = httptest.NewRecorder()
	request = httptest.NewRequest("GET", "/5ebe2294ecd0e0f08eab7690d2a6ee69", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusOK {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusOK, writer.Code)
	}

	expected = `{"data":"secret"}`
	got = writer.Body.String()
	if expected != got {
		t.Errorf("Expected Response Body to be: %s, Got: %s", expected, got)
	}

	// Retrieve secret second time
	writer = httptest.NewRecorder()
	request = httptest.NewRequest("GET", "/5ebe2294ecd0e0f08eab7690d2a6ee69", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusNotFound {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusNotFound, writer.Code)
	}

	expected = `{"data":""}`
	got = writer.Body.String()
	if expected != got {
		t.Errorf("Expected Response Body to be: %s, Got: %s", expected, got)
	}
}
