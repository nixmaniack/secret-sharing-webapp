package handlers

import "net/http"

// RegisterHandlers registers API handlers
func RegisterHandlers(mux *http.ServeMux) {
	mux.HandleFunc("/healthcheck", healthcheckHandler)
	mux.HandleFunc("/", secretHandler)
}
