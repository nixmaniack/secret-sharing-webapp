package handlers

var secretsDatabase = make(map[string]string)

type secretRequest struct {
	PlainText string `json:"plain_text"`
}

type secretResponse struct {
	Id string `json:"id"`
}

type secretData struct {
	Data string `json:"data"`
}
