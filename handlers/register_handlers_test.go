package handlers

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var mux *http.ServeMux
var writer *httptest.ResponseRecorder

func TestMain(m *testing.M) {
	setUp()
	exitCode := m.Run()
	os.Exit(exitCode)
}

func setUp() {
	mux = http.NewServeMux()
	mux.HandleFunc("/healthcheck", healthcheckHandler)
	mux.HandleFunc("/", secretHandler)
}

func TestHealthCheck(t *testing.T) {
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/healthcheck", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusOK {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusOK, writer.Code)
	}

	if writer.Body.String() != "ok\n" {
		t.Errorf("Expected: %s, Got: %s", "ok\n", writer.Body.String())
	}
}

func TestSecretHandlerGETNotFound(t *testing.T) {
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("GET", "/", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusNotFound {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusNotFound, writer.Code)
	}

	expected := `{"data":""}`
	got := writer.Body.String()
	if expected != got {
		t.Errorf("Expected Response Body to be: %s, Got: %s", expected, got)
	}
}

func TestSecretHandlerPOSTEmptyJSON(t *testing.T) {
	writer := httptest.NewRecorder()
	request := httptest.NewRequest("POST", "/", nil)
	mux.ServeHTTP(writer, request)

	if writer.Code != http.StatusBadRequest {
		t.Errorf("Expected Response Status to be: %d, Got: %d", http.StatusBadRequest, writer.Code)
	}
}
