package handlers

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

func secretHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		getSecret(w, r)
	case http.MethodPost:
		createSecret(w, r)
	default:
		http.Error(w, "Method not supported", http.StatusMethodNotAllowed)
	}
}

func createSecret(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintln(w, "POST createSecret")
	var sreq secretRequest
	err := json.NewDecoder(r.Body).Decode(&sreq)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if sreq.PlainText == "" {
		http.Error(w, "Invalid request", http.StatusBadRequest)
		return
	}

	md5sum := fmt.Sprintf("%x", md5.Sum([]byte(sreq.PlainText)))
	resp := secretResponse{Id: md5sum}
	secretsDatabase[md5sum] = sreq.PlainText
	js, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func getSecret(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Path
	id = strings.TrimPrefix(id, "/")
	resp := secretData{}
	secret, ok := secretsDatabase[id]
	if !ok {
		// w.WriteHeader(http.StatusNotFound)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(404)
		resp.Data = ""
		js, _ := json.Marshal(resp)
		w.Write(js)
		return
	}
	resp.Data = secret
	delete(secretsDatabase, id)
	js, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
